/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.ls.LSInput;

/**
 * Class representing an ECH definition input.
 */
public class EchInput implements LSInput {

  private static final Logger logger = LoggerFactory.getLogger(EchInput.class);

  private String      publicId;
  private String      systemId;
  private InputStream inputStream;

  public EchInput(String publicId, String sysId, InputStream input) {
    this.publicId = publicId;
    this.systemId = sysId;
    this.inputStream = input;
  }

  public String getPublicId() {
    return publicId;
  }

  public void setPublicId(String publicId) {
    this.publicId = publicId;
  }

  public String getSystemId() {
    return systemId;
  }

  public void setSystemId(String systemId) {
    this.systemId = systemId;
  }

  public String getStringData() {
    try (BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
      return buffer.lines().collect(Collectors.joining("\n"));
    } catch (IOException e) {
      logger.warn("error while parsing ECH definition", e);
      return null;
    }
  }

  public void setStringData(String stringData) {
    throw new UnsupportedOperationException();
  }

  public String getBaseURI() {
    return null;
  }

  public void setBaseURI(String baseURI) {
    throw new UnsupportedOperationException();
  }

  public InputStream getByteStream() {
    return null;
  }

  public void setByteStream(InputStream byteStream) {
    throw new UnsupportedOperationException();
  }

  public boolean getCertifiedText() {
    return false;
  }

  public void setCertifiedText(boolean certifiedText) {
    throw new UnsupportedOperationException();
  }

  public Reader getCharacterStream() {
    return null;
  }

  public void setCharacterStream(Reader characterStream) {
    throw new UnsupportedOperationException();
  }

  public String getEncoding() {
    return null;
  }

  public void setEncoding(String encoding) {
    throw new UnsupportedOperationException();
  }
}