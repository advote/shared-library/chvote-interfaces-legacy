/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.service;

import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.stream.XMLStreamReader;

/**
 * Ech encoder/decoder.
 *
 * @param <T> the type of objects being encoded/decoded.
 */
public interface EchCodec<T> {

  /**
   * Validates the given eCH XML stream and deserializes it into the corresponding Java object.
   *
   * @param stream the stream to read from.
   *
   * @return the deserialized object.
   *
   * @throws EchDeserializationRuntimeException if the stream can't be parsed.
   */
  default T deserialize(InputStream stream) {
    return deserialize(stream, true);
  }

  /**
   * Reads the given eCH XML stream and deserializes it into the corresponding Java object.
   *
   * @param stream   the stream to read from.
   * @param validate whether the input xml must be validated during the process.
   *
   * @return the deserialized object.
   *
   * @throws EchDeserializationRuntimeException if the stream can't be parsed.
   */
  T deserialize(InputStream stream, boolean validate);

  /**
   * Serializes and validates the given eCH object representation and writes it to the specified stream.
   *
   * @param ech    the eCH object to serialize.
   * @param stream the stream to write to.
   *
   * @throws EchSerializationRuntimeException if the given object can't be serialized.
   */
  default void serialize(T ech, OutputStream stream) {
    serialize(ech, stream, true);
  }

  T deserialize(XMLStreamReader streamReader, boolean validate);

  /**
   * Serializes the given eCH object representation and writes it to the specified stream.
   *
   * @param ech      the eCH object to serialize.
   * @param stream   the stream to write to.
   * @param validate whether the output xml must be validated during the process.
   *
   * @throws EchSerializationRuntimeException if the given object can't be serialized.
   */
  void serialize(T ech, OutputStream stream, boolean validate);
}
