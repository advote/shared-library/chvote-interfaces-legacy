/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.service;

import ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery;
import java.io.OutputStream;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;

public final class VotationReferentialFactory extends JAXBEchCodecImpl<Delivery> {
  private static final String CDATA_XML_ELEMENTS = "ballotQuestion tieBreakQuestion";

  private final TransformerFactory     transformerFactory;
  private final DocumentBuilderFactory documentBuilderFactory;

  public VotationReferentialFactory() {
    super(Delivery.class);
    this.transformerFactory = safeTransformerFactory();
    this.documentBuilderFactory = safeDocumentBuilderFactory();
  }

  private TransformerFactory safeTransformerFactory() {
    TransformerFactory tf = TransformerFactory.newInstance();
    tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
    tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
    return tf;
  }

  private DocumentBuilderFactory safeDocumentBuilderFactory() {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

    // XXE prevention: see https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.md

    // per Timothy Morgan's 2014 paper: "XML Schema, DTD, and Entity Attacks"
    dbf.setXIncludeAware(false);
    dbf.setExpandEntityReferences(false);

    String feature = null;
    try {
      // This is the PRIMARY defense. If DTDs (doctypes) are disallowed, almost all
      // XML entity attacks are prevented
      // Xerces 2 only - http://xerces.apache.org/xerces2-j/features.html#disallow-doctype-decl
      feature = "http://apache.org/xml/features/disallow-doctype-decl";
      dbf.setFeature(feature, true);

      // If you can't completely disable DTDs, then at least do the following:
      // Xerces 1 - http://xerces.apache.org/xerces-j/features.html#external-general-entities
      // Xerces 2 - http://xerces.apache.org/xerces2-j/features.html#external-general-entities
      // JDK7+ - http://xml.org/sax/features/external-general-entities
      feature = "http://xml.org/sax/features/external-general-entities";
      dbf.setFeature(feature, false);

      // Xerces 1 - http://xerces.apache.org/xerces-j/features.html#external-parameter-entities
      // Xerces 2 - http://xerces.apache.org/xerces2-j/features.html#external-parameter-entities
      // JDK7+ - http://xml.org/sax/features/external-parameter-entities
      feature = "http://xml.org/sax/features/external-parameter-entities";
      dbf.setFeature(feature, false);

      // Disable external DTDs as well
      feature = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
      dbf.setFeature(feature, false);

      // Process XML securely feature
      feature = XMLConstants.FEATURE_SECURE_PROCESSING;
      dbf.setFeature(feature, true);
    } catch (ParserConfigurationException e) {
      // This should catch a failed setFeature feature
      throw new EchDeserializationRuntimeException("ParserConfigurationException was thrown. The feature '" + feature
                  + "' is probably not supported by your XML processor.", e);
    }

    return dbf;
  }

  @Override
  public void serialize(Delivery delivery, OutputStream stream, boolean validate) {
    try {
      Document document = createDocument();
      final Marshaller marshaller = getContext().createMarshaller();
      if (validate) {
        marshaller.setSchema(EchSchema.get());
      }
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      marshaller.marshal(delivery, document);
      createTransformer().transform(new DOMSource(document), new StreamResult(stream));
    } catch (TransformerException | ParserConfigurationException | JAXBException e) {
      throw new EchSerializationRuntimeException(e);
    }
  }

  private Transformer createTransformer() throws JAXBException {
    Transformer transformer;
    try {
      transformer = transformerFactory.newTransformer();
    } catch (TransformerConfigurationException e) {
      throw new JAXBException(e);
    }
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
    transformer.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS, CDATA_XML_ELEMENTS);
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
    return transformer;
  }

  private Document createDocument() throws ParserConfigurationException {
    final Document document = documentBuilderFactory.newDocumentBuilder().newDocument();
    document.setXmlStandalone(true);
    return document;
  }
}
