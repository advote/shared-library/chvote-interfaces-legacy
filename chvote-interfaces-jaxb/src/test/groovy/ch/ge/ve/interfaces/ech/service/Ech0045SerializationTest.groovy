/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.service

import ch.ge.ve.interfaces.ech.eCH0045.v4.VoterDelivery
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Specification

import java.nio.charset.Charset

class Ech0045SerializationTest  extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-eCH-0045-v4.xml"

  def factory = new JAXBEchCodecImpl<VoterDelivery>(VoterDelivery.class)

  def "a valid eCH-0045 file should be successfully parsed"() {
    given: 'a valid eCH-0045 XML file'
    InputStream stream = getClass().getResourceAsStream(SAMPLE_FILE_PATH)

    when:
    VoterDelivery delivery = factory.deserialize(stream, false)

    then: 'the file should be correctly parsed'
    delivery != null

    and: "it should contain a single voter"
    def voters = delivery.voterList.voter
    voters.size() == 1

    and: "it should contain the voter's postage code as an extension"
    def voter = voters.get(0)
    def extension = voter.person.swiss.swissDomesticPerson.extension
    extension != null
    extension.postageCode == 1
  }

  def "a valid delivery should be successfully serialized to xml"() {
    given: 'a delivery generated from a valid eCH-0045 file'
    VoterDelivery delivery = factory.deserialize(getClass().getResourceAsStream(SAMPLE_FILE_PATH))

    when: 'serializing the delivery read from the eCH-0045 file'
    OutputStream out = new ByteArrayOutputStream()
    factory.serialize(delivery, out)

    then: 'serialization output should be similar to the original file'
    Assert.assertThat(Input.from(new String(out.toByteArray(), Charset.forName("UTF-8"))),
                      XmlMatchers.isSimilarTo(Input.fromStream(getClass().getResourceAsStream(SAMPLE_FILE_PATH))))
  }
}
