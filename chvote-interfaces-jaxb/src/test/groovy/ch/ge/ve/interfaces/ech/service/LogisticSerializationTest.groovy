/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-interfaces                                                                              -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.interfaces.ech.service

import static ch.ge.ve.interfaces.ech.parser.LocalDateTimeTestUtilities.localDateTime

import ch.ge.ve.interfaces.ech.eCH0155.v4.DomainOfInfluenceTypeType
import ch.ge.ve.interfaces.logistic.Delivery
import org.junit.Assert
import org.xmlunit.builder.Input
import spock.lang.Specification

class LogisticSerializationTest extends Specification {

  private static final String SAMPLE_FILE_PATH = "/sample-logistic-v1.xml"

  def factory = new JAXBEchCodecImpl<Delivery>(Delivery.class)

  def "valid XML file should contain expected elements"() {
    given: 'a valid XML file'
    InputStream stream = this.getClass().getResourceAsStream(SAMPLE_FILE_PATH)

    when: 'unmarshalling file content'
    Delivery delivery = factory.deserialize(stream)

    then: 'content should be valid'
    delivery != null
    delivery.getDeliveryHeader().getSenderId() == 'bo://TEST'
    delivery.getDeliveryHeader().getMessageId() == '70dc0e5d947d44bdbbef2092ee473f99'
    delivery.getDeliveryHeader().getMessageDate() == localDateTime('25.07.2017 14:00:00')
    def domainOfInfluences = delivery.getDomainOfInfluence()
    domainOfInfluences.size() == 3
    domainOfInfluences[0].getDomainOfInfluenceType() == DomainOfInfluenceTypeType.CH
    domainOfInfluences[0].getLocalDomainOfInfluenceIdentification() == '1'
    domainOfInfluences[0].getDomainOfInfluenceName() == 'Confédération'
    domainOfInfluences[0].getDomainOfInfluenceShortname() == 'CH'
    domainOfInfluences[1].getDomainOfInfluenceType() == DomainOfInfluenceTypeType.CT
    domainOfInfluences[1].getLocalDomainOfInfluenceIdentification() == '1'
    domainOfInfluences[1].getDomainOfInfluenceName() == 'Canton de Zürich'
    domainOfInfluences[1].getDomainOfInfluenceShortname() == 'ZH'
    domainOfInfluences[2].getDomainOfInfluenceType() == DomainOfInfluenceTypeType.MU
    domainOfInfluences[2].getLocalDomainOfInfluenceIdentification() == '261'
    domainOfInfluences[2].getDomainOfInfluenceName() == 'Ville de Zürich'
    domainOfInfluences[2].getDomainOfInfluenceShortname() == 'Züri'
  }

  def "a logistic delivery can be converted to xml"() {
    given: 'a valid delivery generated from an XML file'
    Delivery delivery = factory.deserialize(this.getClass().getResourceAsStream(SAMPLE_FILE_PATH))

    when: 'marshalling'
    OutputStream out = new ByteArrayOutputStream()
    factory.serialize(delivery, out)

    then: 'content should be valid'
    Assert.assertThat(Input.from(new String(out.toByteArray(), "UTF-8")),
                      XmlMatchers.isSimilarTo(Input.fromStream(getClass().getResourceAsStream(SAMPLE_FILE_PATH))))
  }
}
